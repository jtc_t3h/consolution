create database consolution;
use consolution;

create table company(
  id int NOT NULL AUTO_INCREMENT,
  address varchar(200),
  phone varchar(20),
  email varchar(100),
  status varchar(1),
  PRIMARY KEY (id)
);

insert into company(address, phone, email, status)
values ('198 West 21th Street, Suite 721 New York NY 10016', '+ 1235 2355 98', 'info@yoursite.com', '1');

create table project(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100),
  image_name varchar(100),
  PRIMARY KEY (id)
);

insert into project(name, image_name)
values ('Brand and Illutration design', 'project-2.jpg');

insert into project(name, image_name)
values ('Human resource website', 'project-1.jpg');
       
