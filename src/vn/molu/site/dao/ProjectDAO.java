package vn.molu.site.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import vn.molu.site.domain.Project;

public class ProjectDAO {

	public List<Project> findAll(){
		SessionFactory factory = HibernateUtil.getSessionfactory();
		Session session = factory.openSession();
		
//		Query query = session.createSQLQuery("select * from project");
		Query query = session.createQuery("from Project");
		
		return query.getResultList();
				
	}
	
	public BigDecimal count(){
		SessionFactory factory = HibernateUtil.getSessionfactory();
		Session session = factory.openSession();
		
		Query query = session.createSQLQuery("select count(*) from project");
		return (BigDecimal)query.getSingleResult();
				
	}
}
