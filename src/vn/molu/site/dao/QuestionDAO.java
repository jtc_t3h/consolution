package vn.molu.site.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import vn.molu.site.domain.Question;

public class QuestionDAO {

	public List<Question> find(int ... startIndexAndLength) throws SQLException{
		String selectSQL = "";
		if (startIndexAndLength.length == 2) {
			selectSQL = "select * from question limit " + startIndexAndLength[0] + "," + startIndexAndLength[1]; 
		} else {
			selectSQL = "select * from question";
		}
		
		String URL = "jdbc:mysql://localhost:3306/consolution";
		String username = "root";
		String password = "";
		
		List<Question> list = new ArrayList<Question>();
		try (Connection con = DriverManager.getConnection(URL, username, password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(selectSQL)){
			
			while(rs.next()) {
			   Question question = new Question();
			   
			   question.setId(rs.getLong("id"));
			   question.setCusName(rs.getString("cus_name"));
			   // continue
			   
			   list.add(question);
			}
		} catch (SQLException e) {
			throw e;
		}
		return list;
	}
}
