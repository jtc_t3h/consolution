package vn.molu.site.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import vn.molu.site.domain.Company;

public class CompanyDAO {

	public Company getInfo() throws SQLException {
		String selectSQL = "select * from company where status = '1' ";
		
		String JDBC_CLASS = "com.mysql.cj.jdbc.Driver";
		try {
			Class.forName(JDBC_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		String URL = "jdbc:mysql://localhost:3306/consolation";
		String username = "root";
		String password = "";
		
		try (Connection con = DriverManager.getConnection(URL, username, password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(selectSQL)){
			
			while(rs.next()) {
			   Company company = new Company();
			   
			   company.setId(rs.getLong("id"));
			   company.setAddress(rs.getString("address"));
			   company.setPhone(rs.getString("phone"));
			   company.setEmail(rs.getString("email"));
			   company.setStatus(rs.getString("status"));

			   return company;
			}
		} catch (SQLException e) {
			throw e;
		}
		return null;
	}
}
